/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         Main.cpp
  Objective:    Serves as control of the test program
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#include "Simulation.h"

int Options();

int main(){

	Simulation s("Carts.txt", "Trains.txt", "Stations.txt");

	while(s.running())
	{
		s.printTrains();
		
		switch (int c=Options())
		{
		case 1: std::cin.clear();
			std::cin.ignore(std::cin.rdbuf()->in_avail());
			s.AdvanceSim();
			break;
		case 2: std::cin.clear();
			std::cin.ignore(std::cin.rdbuf()->in_avail());
			s.searchCart();
			break;
		case 3: std::cin.clear();
			std::cin.ignore(std::cin.rdbuf()->in_avail());
			s.printTrainCartInfo();
			break;
		case 4: s.setSimTimeInterval();
			break;
		case 5: std::cin.clear();
			std::cin.ignore(std::cin.rdbuf()->in_avail());
			s.printCartsFromAllStations();
			break;
		case 6: std::cin.clear();
			std::cin.ignore(std::cin.rdbuf()->in_avail());
			s.EndSim();
			break;
		default:std::cout << "Invalid Input, please try again\n";
			c=Options();
			break;
		}
		std::cout << "\n\n\n\n\n\n\n\n\n\n";
	}

	return 0;
}

int Options()
{
	std::cout << "\n\n1. Advance\n2. Search cart, by cartID\n3. Search train route ID, and see info for (un)assembled carts\n4. Set new SimTimeInterval\n5. Print cartPools\n6. Exit/EndSim\n\nChoice: ";
	int c;
	std::cin >> c;
	return c;
}