/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         Simulation.cpp
  Objective:    Implementation of Simulation class
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#include "Simulation.h"

const char DELIM = '|';

Simulation::Simulation(std::string pCartsFN,std::string pTrainsFN,std::string pStationsFN)
{
	SimTime=0;
	SimTimeInterval=10;
	SimEndTime=1440;//run until 24:00

	readStations(pStationsFN);
	readTrains(pTrainsFN);
	readCarts(pCartsFN);

	SimTime=optimalStartTime();
};

Simulation::~Simulation()
{
	for(std::vector<Train*>::iterator tIt=AllTrains.begin(); tIt!=AllTrains.end();++tIt)
	{
		delete *tIt;
	}
	AllTrains.clear();
	
	for(std::vector<Station*>::iterator sIt=AllStations.begin(); sIt!=AllStations.end();++sIt)
	{
		delete *sIt;
	}
	AllStations.clear();
}

void Simulation::readStations(std::string sFile)
{
	std::ifstream stationsFile(sFile);
	std::string inLine;

	while(std::getline(stationsFile, inLine))
	{
		AllStations.push_back(new Station(inLine));
	}

	stationsFile.close();
}

void Simulation::readTrains(std::string tFile)
{
	std::ifstream trainsFile(tFile);
	std::ofstream errorFile("unread_trains.txt");
	std::string tID, tDT, tAT, tOS, tDS;
	std::string inLine, temp, error;

	while(std::getline(trainsFile, tID, DELIM))
	{
		if(dupeTrainID(tID))
		{
			error = "Couldn't read Train: ID not unique.";
			errorFile << "Error on ID " << tID << ". " << error << std::endl;
			std::getline(trainsFile, temp);
			continue;
		}

		std::getline(trainsFile, tDT, DELIM);
		std::getline(trainsFile, tAT, DELIM);
		std::getline(trainsFile, tOS, DELIM);
		std::getline(trainsFile, tDS, DELIM);

		if(findStationPtr(tOS) == nullptr || findStationPtr(tDS) == nullptr)
		{
			error = "Couldn't read Train: No such station.";
			errorFile << "Error on ID " << tID << ". " << error << std::endl;
			std::getline(trainsFile, temp);
			continue;
		}

		Train *t = new Train(tID, findStationPtr(tOS), findStationPtr(tDS), atoi(tDT.c_str()), atoi(tAT.c_str()));

		std::string cartString;
		std::getline(trainsFile, temp);
		std::istringstream line(temp);

		while(line >> cartString)
		{
			if(validateCart(cartString))//if it is not a valid cartname/type it will be dropped/forgotten
			{
				t->addMissingCart(cartString);
			}
		}

		AllTrains.push_back(t);

	}
	errorFile.close();
	trainsFile.close();
}

void Simulation::readCarts(std::string cFile)
{
	std::ifstream cartsFile(cFile);
	std::ofstream errorFile("unread_carts.txt");
	std::string cID, cType, cSpeed, cWattage, cMileage, cCapacity,
				cArea, cVolume, cRoom, cInternet, cStation;
	std::string inLine, error, temp;

	while(std::getline(cartsFile, cType, DELIM))
	{
		std::getline(cartsFile, cID, DELIM);

		if(dupeCartID(cID))
		{
			error = "Couldn't read Cart: ID not unique.";
			errorFile << "Error on ID " << cID << ". " << error << std::endl;
			std::getline(cartsFile, temp);
			continue;
		}

		error = "Couldn't read Cart: No such station.";

		if(cType == "ElLoc")
		{
			std::getline(cartsFile, cSpeed, DELIM);
			std::getline(cartsFile, cWattage, DELIM);
			std::getline(cartsFile, cStation);

			if(findStationPtr(cStation) == nullptr)
			{
				errorFile << "error on ID " << cID << ". " << error << std::endl;
				std::getline(cartsFile, temp);
				continue;
			}

			findStationPtr(cStation)->addCartPointer(new ElLoc(cID, cSpeed, cWattage));

		}
		
		else if(cType == "DieselLoc")
		{
			std::getline(cartsFile, cSpeed, DELIM);
			std::getline(cartsFile, cMileage, DELIM);
			std::getline(cartsFile, cStation);


			if(findStationPtr(cStation) == nullptr)
			{
				errorFile << "error on ID " << cID << ". " << error << std::endl;
				std::getline(cartsFile, temp);
				continue;
			}

			findStationPtr(cStation)->addCartPointer(new DieselLoc(cID, cSpeed, cMileage));

		}
		
		else if(cType == "OpenFreight")
		{
			std::getline(cartsFile, cCapacity, DELIM);
			std::getline(cartsFile, cArea, DELIM);
			std::getline(cartsFile, cStation);


			if(findStationPtr(cStation) == nullptr)
			{
				errorFile << "error on ID " << cID << ". " << error << std::endl;
				std::getline(cartsFile, temp);
				continue;
			}

			findStationPtr(cStation)->addCartPointer(new OpenFreight(cID, cCapacity, cArea));
		}
		
		else if(cType == "ClosedFreight")
		{
			std::getline(cartsFile, cVolume, DELIM);
			std::getline(cartsFile, cStation);


			if(findStationPtr(cStation) == nullptr)
			{
				errorFile << "error on ID " << cID << ". " << error << std::endl;
				std::getline(cartsFile, temp);
				continue;
			}

			findStationPtr(cStation)->addCartPointer(new ClosedFreight(cID, cVolume));
		}
		
		else if(cType == "SleepCart")
		{
			std::getline(cartsFile, cRoom, DELIM);
			std::getline(cartsFile, cStation);

			if(findStationPtr(cStation) == nullptr)
			{
				errorFile << "error on ID " << cID << ". " << error << std::endl;
				std::getline(cartsFile, temp);
				continue;
			}

			findStationPtr(cStation)->addCartPointer(new SleepCart(cID, cRoom));
		}
		
		else if(cType == "SitCart")
		{
			std::getline(cartsFile, cRoom, DELIM);
			std::getline(cartsFile, cInternet, DELIM);
			std::getline(cartsFile, cStation);

			if(findStationPtr(cStation) == nullptr)
			{
				errorFile << "error on ID " << cID << ". " << error << std::endl;
				std::getline(cartsFile, temp);
				continue;
			}

			findStationPtr(cStation)->addCartPointer(new SitCart(cID, cRoom, cInternet));
		}

	}
	errorFile.close();
	cartsFile.close();
}


Station* Simulation::findStationPtr(std::string pName)
{
	for(vector<Station*>::iterator it=AllStations.begin();it!=AllStations.end();it++)
	{
		if((*it)->getName()==pName)
		{
			return *it;
		}
	}
	return nullptr;
};

//Advance Simulation by 1 minute
void Simulation::Advance()
{
	SimTime+=1;

	for(std::vector<Train*>::iterator trainsIt=AllTrains.begin();trainsIt!=AllTrains.end();++trainsIt)
	{
		//if supposed to leave in 30 minutes or less, and NOT_ASSEMBLED
		if((*trainsIt)->getDepartTime()-30<=SimTime && (*trainsIt)->getState()==NOT_ASSEMBLED)
		{
			//if not assembled within 10 minutes of depart time
			(*trainsIt)->attemptAssembly();
			
			if((*trainsIt)->getDepartTime()-10==SimTime && (*trainsIt)->getState()==NOT_ASSEMBLED) 
			{
				(*trainsIt)->delayBy10();
			}
		}
		
		//if supposed to leave in 10 minutes, and NOT_READY
		if((*trainsIt)->getDepartTime()-10==SimTime && (*trainsIt)->getState()==NOT_READY) 
		{
			(*trainsIt)->setState(READY);
		}

		if((*trainsIt)->getDepartTime()==SimTime && (*trainsIt)->getState()==READY)
		{
			(*trainsIt)->setState(RUNNING);
		}

		if((*trainsIt)->getArrivalTime()==SimTime && (*trainsIt)->getState()==RUNNING)
		{
			(*trainsIt)->setState(ARRIVED);
		}

		if((*trainsIt)->getArrivalTime()+30<=SimTime && (*trainsIt)->getState()==ARRIVED)
		{
			(*trainsIt)->disassemble();
		}
	}

	if(SimTime==SimEndTime || allFinished())
	{
		EndSim();
	}
};

void Simulation::AdvanceSim()
{
	int tmp=0;
	while(tmp<SimTimeInterval)
	{
		Advance();
		tmp++;
	}
}

void Simulation::printTrains()
{
	std::cout << "CURRENT SimTime: " << Train::minutes_to_time(SimTime) 
		      << "\nCurrent SimTimeInterval: " << SimTimeInterval << "minutes.\n\n";

	std::cout << std::left << std::setw(8)<< "RouteID" << std::setw(20)
		      << "Origin" << std::setw(15) << "Destination" 
			  << std::setw(10) << "Dep. Time" << std::setw(10) 
			  << "Arr. Time" << std::setw(10) << "STATE" << "\n";

	for(std::vector<Train*>::const_iterator it=AllTrains.begin(); it!=AllTrains.end();++it)
	{
		(*it)->printRouteInfo();
	}
}

void Simulation::searchCart()
{
	std::cout << "Please input the ID of the cart you are searching for: ";
	std::string cID;
	std::getline(std::cin,cID);
	Cart* cartRef;

	for(std::vector<Train*>::const_iterator tIt=AllTrains.begin(); tIt!=AllTrains.end();++tIt)
	{
		if((*tIt)->searchCart(cID,cartRef))
		{
			std::cout << "Cart " << cartRef->getID() 
				      << " was found assembled in train route " 
					  << (*tIt)->getId() << ", and is of type " 
					  << typeid(*cartRef).name() << "\nMore info about cart: \n";

			cartRef->print();
			std::cin.get();
			return;
		}
	}

	for(std::vector<Station*>::const_iterator sIt=AllStations.begin(); sIt!=AllStations.end();++sIt)
	{
		if((*sIt)->searchCart(cID,cartRef))
		{
			std::cout << "Cart " << cartRef->getID() << " was found at station " 
				      << (*sIt)->getName() << ", and is of type " 
					  << typeid(*cartRef).name() << "\n More info about cart: \n";

			cartRef->print();
			std::cin.get();
			return;
		}
	}

	std::cout << cID << " not found anywhere.\n";
	std::cin.get();
	return;
}

void Simulation::printTrainCartInfo()
{
	std::cout << "Which train do you want to inspect: ";
	std::string tID;
	std::getline(std::cin,tID);

	for(std::vector<Train*>::const_iterator tIt=AllTrains.begin(); tIt!=AllTrains.end();++tIt)
	{
		if((*tIt)->getId()==tID)
		{
			if((*tIt)->getState()==NOT_ASSEMBLED)
			{
				(*tIt)->printMissingCarts();
			}
			else
			{
				std::cout << "\nAssembled carts:\n";
				(*tIt)->printCarts();
			}
			std::cin.get();
			return;
		}
	}

	std::cout << "Train not found.\n";
	std::cin.get();
	return;
}

void Simulation::EndSim()
{
	std::cout << "\n\n == Simulation ended. Final states and delay statistics are displayed. ==\n\n";
	printTrains();//output one more time, for the user to see the last states

	SimTime=SimEndTime;//Simulation not running anymore
	int successfullRoutes=0,neverLeft=0,delayTotal=0,delayedTrains=0,stillRunning=0;

	for(std::vector<Train*>::const_iterator tIt=AllTrains.begin(); tIt!=AllTrains.end();++tIt)
	{
		if((*tIt)->getState()==ARRIVED || (*tIt)->getState()== FINISHED)
		{
			successfullRoutes+=1;
		}

		if((*tIt)->getState()==NOT_ASSEMBLED || (*tIt)->getState()==NOT_READY || (*tIt)->getState()==READY)
		{
			neverLeft+=1;
		}

		if((*tIt)->getState()==RUNNING)
		{
			stillRunning+=1;
		}

		if((*tIt)->getTotalDelay()>0)
		{
			delayedTrains+=1;
			delayTotal+=(*tIt)->getTotalDelay(); //add the difference
		}
	}

	std::cout << "\nSuccessful routes: " << successfullRoutes << "\n"
		<< "Trains still running: " << stillRunning << "\n"
		<< "Trains that never left / were not fully assembled: " << neverLeft << "\n"
		<< "Delayed trains: " << delayedTrains << "\n"
		<< "Total delay: " << Train::minutes_to_time(delayTotal) 
		<< " (" << delayTotal  << ")\n\nPress any key to exit";

	std::cin.get();
}

void Simulation::setSimTimeInterval()
{
	std::cout << "How many minutes do you want to advance each time? ";
	int newInterval;
	std::cin >> newInterval;
	SimTimeInterval=newInterval;
}

bool Simulation::dupeCartID(std::string pID)
{
	Cart* cartRef;

	for(std::vector<Train*>::const_iterator tIt=AllTrains.begin(); tIt!=AllTrains.end();++tIt)
	{
		if((*tIt)->searchCart(pID,cartRef))
		{
			return true;
		}
	}

	for(std::vector<Station*>::const_iterator sIt=AllStations.begin(); sIt!=AllStations.end();++sIt)
	{
		if((*sIt)->searchCart(pID,cartRef))
		{
			return true;
		}
	}

	return false;
}

bool Simulation::dupeTrainID(std::string pID)
{
	for(std::vector<Train*>::const_iterator tIt=AllTrains.begin(); tIt!=AllTrains.end();++tIt)
	{
		if((*tIt)->getId()==pID)
		{
			return true;
		}
	}
	return false;
}

bool Simulation::validateCart(std::string &vCart)
{
	if(vCart=="SitCart")
	{
		vCart=typeid(SitCart).name();
		return true;
	}
	if(vCart=="SleepCart")
	{
		vCart=typeid(SleepCart).name();
		return true;
	}
	if(vCart=="ClosedFreight")
	{
		vCart=typeid(ClosedFreight).name();
		return true;
	}
	if(vCart=="OpenFreight")
	{
		vCart=typeid(OpenFreight).name();
		return true;
	}
	if(vCart=="DieselLoc")
	{
		vCart=typeid(DieselLoc).name();
		return true;
	}
	if(vCart=="ElLoc")
	{
		vCart=typeid(ElLoc).name();
		return true;
	}
	return false;
}

void Simulation::printCartsFromAllStations()
{
	for(std::vector<Station*>::const_iterator sIt=AllStations.begin(); sIt!=AllStations.end();++sIt)
	{
		std::cout << (*sIt)->getName() << " station cartPool";
		(*sIt)->printCarts();
		
		if(sIt!=--AllStations.end())
		{
			std::cout << "Press any key to continue to the next station... ";
			std::cin.get();
		}
	}
	std::cout << "All cartPools printed, press any key to continue... ";
	std::cin.get();
}

int Simulation::optimalStartTime()
{
	//no delays should have been applied yet
	int firstEvent=AllTrains[0]->getDepartTime();

	for(std::vector<Train*>::const_iterator tIt=AllTrains.begin();tIt!=AllTrains.end();++tIt)
	{
		if(firstEvent>(*tIt)->getDepartTime())
		{
			firstEvent=(*tIt)->getDepartTime();
		}
	}
	
	firstEvent-=30;
	if(firstEvent<0)
	{
		return 0;
	}
	else
	{
		return firstEvent;
	}
}

bool Simulation::allFinished()
{
	bool allFinished=true;
	for(std::vector<Train*>::const_iterator tIt=AllTrains.begin();tIt!=AllTrains.end();++tIt)
	{
		if((*tIt)->getState()!=FINISHED)
		{
			allFinished=false;
		}
	}
	return allFinished;
}