/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         Simulation.h
  Objective:    Header for Simulation class
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#ifndef SimulationH
#define SimulationH

#include "Train.h"
#include <fstream>
#include <sstream>
#include <stdlib.h>

class Simulation
{
private:
	std::vector<Train*> AllTrains;
	std::vector<Station*> AllStations;
	int SimTime;	// in minutes
	int SimTimeInterval;
	int SimEndTime; // in minutes

	Station* findStationPtr(std::string pName);

public:
	Simulation(std::string pCartsFN,std::string pTrainsFN,std::string pStationsFN);
	~Simulation();

	void readStations(std::string sFile);
	void readTrains(std::string tFile);
	void readCarts(std::string cFile);
	void Advance();
	void AdvanceSim();
	void EndSim();

	bool running() { return SimTime<SimEndTime;}

	void printTrains();
	void printTrainCartInfo();

	void searchCart();
	bool dupeCartID(std::string pID);
	bool dupeTrainID(std::string pID);
	void setSimTimeInterval();
	bool validateCart(std::string &vCard);
	void printCartsFromAllStations();
	int optimalStartTime();
	bool allFinished();
};

#endif
