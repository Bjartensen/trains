/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         Station.cpp
  Objective:    Implementation of Station class
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#include "Station.h"

Station::~Station()
{
	for(std::vector<Cart*>::iterator cpIt=cartPool.begin();cpIt!=cartPool.end();++cpIt)
	{
		delete *cpIt;
	}
	cartPool.clear();
}


bool Station::findCart(std::string type,Cart* &cartP)
{
	for(std::vector<Cart*>::iterator cpIt=cartPool.begin();cpIt!=cartPool.end();++cpIt)
	{
		if(type==typeid(**cpIt).name())
		{
			cartP=*cpIt;
			cartPool.erase(cpIt);
			return true;
		}
	}
	return false;
}

bool Station::searchCart(std::string pId,Cart* &cartRef)
{
	for(std::vector<Cart*>::iterator cartIt=cartPool.begin();cartIt!=cartPool.end();++cartIt)
	{
		if((*cartIt)->getID()==pId)
		{
			cartRef=(*cartIt);
			return true;
		}
	}
	return false;
}

void Station::printCarts()
{
	std::cout << std::setw(10) << "\nCartID" << std::setw(10) << "Cart type\n";
	for(std::vector<Cart*>::iterator cIt=cartPool.begin();cIt!=cartPool.end();++cIt)
	{
		std::cout << std::setw(9) << (*cIt)->getID() << typeid(**cIt).name() << "\n";
	}
}