/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         Station.h
  Objective:    Header for Station class
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#ifndef StationH
#define StationH

#include "carts.h"
#include <vector>
#include <iomanip>

class Station
{
private:
	std::string name;
	std::vector<Cart*> cartPool;

public:
    Station(std::string pName) {name=pName;}
	~Station();

    void addCartPointer(Cart* pCartP) {cartPool.push_back(pCartP);}
	std::string getName() {return name;}
	bool findCart(std::string type,Cart* &cartP);
	bool searchCart(std::string cID,Cart* &cartP);
	void printCarts();
};

#endif
