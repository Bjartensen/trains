/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         Train.cpp
  Objective:    Implementation of Train class
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#include "Train.h"


Train::Train(std::string pID,Station* pOrig,Station* pDest,int pDepartTime,int pArrivaleTime)
{
	state=NOT_ASSEMBLED;
	ID=pID;
	origin=pOrig;
	destination=pDest;
	departTime=pDepartTime;
	arrivalTime=pArrivaleTime;
	delay=0;
}

Train::~Train()
{
	for(std::vector<Cart*>::iterator it=assembledCarts.begin();it!=assembledCarts.end();it++)
	{
		delete *it;
	}
	assembledCarts.clear();
}

void Train::attemptAssembly()
{
	if(missingCarts.empty())
	{
		state=NOT_READY;
	}
	else
	{
		vector<std::string>::iterator it=missingCarts.begin();
		while(it!=missingCarts.end()) //
		{
			Cart* cartP;
			if(origin->findCart(*it,cartP))
			{
				assembledCarts.push_back(cartP);
				it=missingCarts.erase(it);
			}
			else
			{
				++it;
			}
		}
		if(missingCarts.empty())
		{
			state=NOT_READY;
		}
		else
		{
			state=NOT_ASSEMBLED;
		}
	}
}

void Train::disassemble()
{
	for(std::vector<Cart*>::const_iterator cartIt=assembledCarts.begin();cartIt!=assembledCarts.end();++cartIt)
	{
		destination->addCartPointer(*cartIt);
	}
	assembledCarts.clear();
	state=FINISHED;
}

void Train::printRouteInfo()
{
	std::string train_states_strings[] = {"NOT_ASSEMBLED","NOT_READY","READY","RUNNING","ARRIVED","FINISHED"};
	
	std::cout << std::left << std::setw(8) << ID 
			  << std::setw(20) << origin->getName()
			  << std::setw(15) << destination->getName()
			  << std::setw(10) << minutes_to_time(departTime) 
			  << std::setw(10) << minutes_to_time(arrivalTime) 
			  << std::setw(15) << train_states_strings[state] << "\n";
}

void Train::printCarts()
{
	std::cout << "Cart ID" << "\tCart type\n";
	for(std::vector<Cart*>::const_iterator cartIt=assembledCarts.begin();cartIt!=assembledCarts.end();++cartIt)
	{
		std::cout << (*cartIt)->getID() << "\t" << typeid(**cartIt).name() << "\n"; //**tada
	}

	std::cout << "\n" << "\n";			
}

std::string Train::minutes_to_time(int pTime)
{
	int hrs = pTime/60;
	int mins = pTime%60;

	std::string tmp = std::to_string(hrs).append(":").append(std::to_string(mins));
	
	if(mins==0)
	{
		tmp.append("0");
	}

	if(hrs<10)
	{
		tmp.insert(0,"0");
	}

	return tmp;
}

bool Train::searchCart(std::string pId,Cart* &cartRef)
{
	for(std::vector<Cart*>::const_iterator cartIt=assembledCarts.begin();cartIt!=assembledCarts.end();++cartIt)
	{
		if((*cartIt)->getID()==pId)
		{
			cartRef=(*cartIt);
			return true;
		}
	}
	return false;
}

void Train::printMissingCarts()
{
	std::cout << "Train " << ID << " is currently missing the following carts:\n";
	for(std::vector<std::string>::const_iterator misIt=missingCarts.begin();misIt!=missingCarts.end();++misIt)
	{
		std::cout << *misIt << "\n";
	}
}