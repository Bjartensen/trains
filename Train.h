/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         Train.h
  Objective:    Header for class Train and train_states enum
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#ifndef TrainH
#define TrainH

#include "Station.h"

using std::vector;


enum train_states
{
	NOT_ASSEMBLED,
	NOT_READY,
	READY,
	RUNNING,
	ARRIVED,
	FINISHED
};

class Train
{
private:
	std::string ID;
	train_states state;
	int departTime;
	int arrivalTime;
	int delay;
	
	std::vector<std::string> missingCarts;
	std::vector<Cart*> assembledCarts;
	Station* origin;
	Station* destination;
	
public:
	Train(std::string pID,Station* pOrig,Station* pDest,int pDepartTime,int pArrivaleTime);
	~Train();

	int getDepartTime() const { return departTime+delay;}
	int getArrivalTime() const { return arrivalTime+delay;}
	int getTotalDelay() const { return delay;}
	train_states getState() { return state;}	
	std::string getId() { return ID;}
	int getNumberOfMissingCarts(){return missingCarts.size();}

	void setState(train_states pState) { state=pState;}

	void addMissingCart(std::string pType)	{ missingCarts.push_back(pType);	}
	

	void attemptAssembly();
	void disassemble();
	void printRouteInfo();
	void printCarts();
	void printMissingCarts();
	static std::string minutes_to_time(int pTime);
	bool searchCart(std::string pId,Cart* &cartRef);
	void delayBy10() {delay+=10;}
};



#endif
