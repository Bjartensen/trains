/*
  Authors		Bjartur Mortensen (bjmo1001), K�ri V�k� (krvi1003)
  File:         carts.h
  Objective:    Header for Cart and derived classes
  Date:         30-11-2013
  Course:		Object-oriented programing in C++, HT13 DT060G
  Laboration:	6 - Trains
*/

#ifndef cartsH
#define cartsH

#include <iostream>
#include <string>

class Cart
{
private:
    std::string uID;
    
public:
    Cart(std::string inID) {uID=inID;}
	virtual ~Cart(){}
    
    std::string getID() {return uID;}
    virtual void print(std::ostream &os = std::cout)
    {os << "Cart train id: " << uID << "\n";}
};


class Loc : virtual public Cart
{
private:
    std::string maxSpeed;
    
public:
    Loc(std::string inID, std::string inSpeed) :Cart(inID), maxSpeed(inSpeed) {}

    virtual void print(std::ostream &os=std::cout)
    {
        Cart::print(os);
        os << "Max speed: " << maxSpeed << "km/h\n";
    }
};


class ElLoc : virtual public Loc
{
private:
    std::string wattage;
    
public:
    ElLoc(std::string inID, std::string inSpeed, std::string inWattage)
    :Cart(inID), Loc(inID,inSpeed), wattage(inWattage) {}

    virtual void print(std::ostream &os=std::cout)
    {   
        Loc::print(os);
        os << "Wattage: " << wattage << "kW\n";
    }
};


class DieselLoc : virtual public Loc
{
private:
    std::string mileage;
    
public:
    DieselLoc(std::string inID, std::string inSpeed, std::string inMileage)
    :Cart(inID), Loc(inID,inSpeed), mileage(inMileage) {} //UGLY

    virtual void print(std::ostream &os=std::cout)
    {
        Loc::print(os);
        os << "Mileage: " << mileage << "L/t\n";
    }
};

class OpenFreight : virtual public Cart
{
private:
    std::string capacity;
    std::string area;
    
public:
    OpenFreight(std::string inID, std::string inCapacity, std::string inArea)
    :Cart(inID), capacity(inCapacity), area(inArea) {}

	virtual void print(std::ostream &os=std::cout)
	{
		Cart::print(os);
		os << "Capacity: " << capacity << "T\n" << "Area: " << area << "Sq.m\n";
	}
};

class ClosedFreight : virtual public Cart
{
private:
    std::string volume;
    
public:
    ClosedFreight(std::string inID, std::string inVol)
    :Cart(inID), volume(inVol) {}

    virtual void print(std::ostream &os=std::cout)
	{
		Cart::print(os);
		os << "Volume: " << volume << "Qube.m\n";
	}
};

class SleepCart : virtual public Cart
{
private:
    std::string room;

public:
	SleepCart(std::string inID, std::string inRoom)
    :Cart(inID), room(inRoom) {}

	virtual void print(std::ostream &os=std::cout)
	{
		Cart::print(os);
		os << "Room: " << room << "Seats/beds\n";
	}
};

class SitCart : virtual public SleepCart
{
private:
	std::string internet;

public:
	SitCart(std::string inID, std::string inRoom, std::string inInternet)
	:Cart(inID), SleepCart(inID,inRoom), internet(inInternet) {}

	virtual void print(std::ostream &os=std::cout)
	{
		SleepCart::print(os);
		os << "Internet: " << internet << "\n";
	}
};


#endif
